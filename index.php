<?php 

    use Psr\Http\Message\ServerRequestInterface as Request;
    use Psr\Http\Message\ResponseInterface as Response;
    use src\Http\Controllers\UserController;

    require_once "vendor/autoload.php";

    function __construct(UserControler $controller) {
        $this->user = $controller;
    }
    
    $app = new \Slim\App();

    $container = $app->getContainer();

    $container['view'] = function ($container) {
        $view = new \Slim\Views\Twig('src/resources/View');
    
        // Instantiate and add Slim specific extension
        $router = $container->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    
        return $view;
    };

    $app->get('/', function (Request $request, Response $response, array $args) {
        return $this->view->render($response, 'index.php');
    });

    $app->post('/', function (Request $request, Response $response, array $args){
        $params = (object) $request->getParams();
        $retorno = $this->User->create($params);
        return $retorno;
    });

    $app->get('/login', function (Request $request, Response $response, array $args){
        return $this->view->render($response, 'login.php');
    });

    $app->post('/login', function (Request $request, Response $response, array $args){
        $retorno = $this->User->login;
        return $retorno;
    });

    $app->run();