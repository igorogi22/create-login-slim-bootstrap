<?php 

namespace src\Httmp\Database;

class AttributesCreate{

	//separar as keys por virgulas preparando para a query
	public function createKeys($attributes){		
		return implode(',',array_keys($attributes));
	}
	//separar os values por virgulas e ':' preparando para a query
	public function createValues($attributes){
		return ':'.implode(',:',array_keys($attributes));
	}
	//preparar para o bind
	public function bindCreateParameters($attributes){
		$values = $this->createValues($attributes);				
		$bindParameters = array_combine(explode(',', $values), array_values($attributes));	
		return $bindParameters;
	}
}