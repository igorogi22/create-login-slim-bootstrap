<?php 
	
namespace src\Http\Database;

use Imodel;
use src\Http\Connection;
use src\Http\Models\AttributesCreate;
use src\Http\Models\AttributesUpdate;
use PDOException;

class AswModel implements Imodel{
	private $database;
	

	public function __construct(){
		$database = new Connection;
		$this->database = $database->connection();
		
	}

	public function create($attributes){

		$attributesCreate = new AttributesCreate;		
		$keys = $attributesCreate->createKeys($attributes);
		$values = $attributesCreate->createValues($attributes);

		$query= "insert into $this->table($keys) values($values)";	

		$pdo = $this->database->prepare($query);

		$bindParameters = $attributesCreate->bindCreateParameters($attributes);		
		try{
			$pdo->execute($bindParameters);
			return $this->database->lastInsertId();
		}
		catch(PDOException $e){
			dump($e->getMessage());
		}
	}
		
	public function read(){
		$query = "select * from $this->table";
		$pdo = $this->database->prepare($query);
		try{
			$pdo->execute();
			return $pdo->fetchAll();
		}
		catch(PDOException $e){
			dump($e->getMessage());
		}
	}
		
	public function update($id, $attributes){
		$attributesUpdate = new AttributesUpdate;
		$fields = $attributesUpdate->updateFields($attributes);
		$query = "update $this->table set $fields where id=:id";
		$pdo = $this->database->prepare($query);
		$bindUpdateParameters = $attributesUpdate->bindUpdateParameters($attributes);
		$bindUpdateParameters['id'] = $id;
		try{
			$pdo->execute($bindUpdateParameters);
			return $pdo->rowCount();			
		}
		catch(PDOException $e){
			dump($e->getMessage());
		}
	}
		
	public function delete($name, $value){
		$query = "delete from $this->table where $name= :$name";
		dump($query);
		$pdo = $this->database->prepare($query);

		try{
			$pdo->bindParam(":$name", $value);
			$pdo->execute();
			return $pdo->rowCount();
		}
		catch(PDOException $e){
			dump($e->getMessage());
		}
	}

		
	public function findBy($name, $value){
		$query = "select * from $this->table where $name= $value";		
		$pdo = $this->database->prepare($query);

		try{		
			$pdo->execute();
			return $pdo->fetch();
		}
		catch(PDOException $e){
			dump($e->getMessage());
		}
	}
}