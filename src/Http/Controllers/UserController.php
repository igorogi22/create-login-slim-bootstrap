<?php

namespace src\Http\Controller;

use src\Http\Repositories\UserRepository;

class UserController {
    
    public function __construct(UserRepository $repository)
	{
		$this->repository = $repository;
	}

	public function create(array $attributes){
		$return = $this->repository->create($attributes);
		return $return;
	}

	public function login($attributes){
		$return = $this->repository->login($attributes);
		return $return;
	}
    
}