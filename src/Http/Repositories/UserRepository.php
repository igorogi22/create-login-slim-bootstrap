<?php

namespace src\Http\Repositories;

use src\Http\Models\User;

class UserRepository {
  private $model;

  public function __construct(User $model){
    $this->user = $model;
  }

  public function create(array $attributes){

    $pass = sha1($attributes["password"]);
    $create = array (
      "email" => $attributes["email"],
      "name" => $attributes["name"],
      "phone" => $attributes["phone"],
      "password" => $pass,
    );

    $return = $user->create($create);

    if($return){
      $_SESSION["mensagen"] = ['status'=>'danger', 'response'=>'Erro ao tentar cadastrar, tente novamente!'];
      header('Location: /');
    }else{
      $_SESSION["mensagen"] = ['status'=>'danger', 'response'=>'Erro ao tentar cadastrar, tente novamente!'];
      header('Location: /');
    }
  }

  public function login(array $attributes){

    $select = $user->findBy('email', $attributes["email"]);
    $pass = sha1($attributes["password"]);

    if($select->email == $attributes["email"] && $select->password==$pass){
      $_SESSION['user'] = $select;
      header('Location: /welcome.php');
    }else{
      $_SESSION['mensage'] = ['status'=>'danger', 'text' =>'Usuario ou senha incorreto, tente novamente!'];
      header('Location: /welcome.php'); 
    }
  }
}